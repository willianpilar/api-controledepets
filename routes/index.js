var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;

var pets = [];

//Confirmar se a API está funcionando
router.get('/health', (req, res) => {
  return res.json({message: 'Aplicação está funcionando!'});
});

//Cadastrar novo Pet
router.post('/pet/new', (req, res) => {
  const { raca, idade, nome, ativo} = req.body;
  
  let id = pets.length + 1; 

  const pet = {
    id,
    raca,
    idade,
    nome,
    ativo
  };

  pets.push(pet);
  return res.json(pet);
});


//Pesquisar Pet pelo nome
router.get('/pet/search', (req, res) => {
  const { petName } = req.body;

  let petReturn = [];

  pets.forEach(petInArray => {
    if (petInArray.nome == petName) {
      petReturn.push(petInArray);
    }
  });

  return res.json(petReturn);

});

//Inativar Pet
router.put('/pet/inactivate', (req, res) => {
  const {id} = req.body;

  let petReturn;
  pets.forEach(petInArray => {
    if (petInArray.id == id) {
      petReturn = petInArray;
    }
  });
  
  petReturn.ativo = false;

  pets[id-1] = petReturn;
  return res.json(pets[id-1]);  

});

//Atualizar dados do Pet
router.put('/pet/update', (req, res) => {
  const {id, raca, idade, nome,} = req.body;

  let petReturn;
  pets.forEach(petInArray => {
    if (petInArray.id == id) {
      petReturn = petInArray;
    }
  });
  
  petReturn.raca = raca;
  petReturn.idade = idade;
  petReturn.nome = nome;

  pets[id-1] = petReturn;
  return res.json(pets[id-1]);

});